package com.dyaadin.odooapps.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.dyaadin.odooapps.R;
import com.dyaadin.odooapps.framework.SessionManager;

public class MainActivity extends AppCompatActivity {
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        session = new SessionManager(getApplicationContext());
    }
}
